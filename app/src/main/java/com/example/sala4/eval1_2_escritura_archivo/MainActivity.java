package com.example.sala4.eval1_2_escritura_archivo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

    TextView tvMostrar;
    @Override
    protected void onPause() {
        super.onPause();
        //Aqui vamos a guardar
        //OutputStrem
        OutputStream os = null;
        try {
            os = openFileOutput("archivo.txt", MODE_PRIVATE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if(os != null){
            //OutputStreamWriter
            OutputStreamWriter osw = new OutputStreamWriter(os);
            //BufferedWriter
            BufferedWriter writer = new BufferedWriter(osw);
            try {
                writer.write("HOLA MUNDO!!");
                writer.flush();
                writer.close();
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvMostrar = (TextView) findViewById(R.id.textMostrar);
        //Abrir el archivo y poner el texto en el textview
        //InputStream
        InputStream file = null;//getResources().openRawResource(R.raw.archivo);
        try {
            file = openFileInput("archivo.txt");
            if(file != null){
                //inputStreamReader
                InputStreamReader isr = new InputStreamReader(file);
                //BufferedReader
                BufferedReader reader = new BufferedReader(isr);
                String text = null;
                try{
                    while((text = reader.readLine()) != null)
                        tvMostrar.append(text + "\n");
                    reader.close();
                    file.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }catch (IOException e){
            e.printStackTrace();
        }

    }
}
